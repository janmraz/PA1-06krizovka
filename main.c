//
//  main.c
//  krizovka
//
//  Created by Jan Mráz on 11/12/2018.
//  Copyright © 2018 Jan Mráz. All rights reserved.
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
    char value;
    char checked;
} LITERAL;

typedef struct {
    char * word;
    char firstChar;
    char lastChar;
    int size;
    int found;
} WORD;

typedef struct {
    LITERAL * line;
    int size;
} LINE;

void freeMemory(WORD * words,int wordsSize,LINE * crosswords, int crosswordHeight, char * line){
    for(int i = 0;i<wordsSize;i++){
        free(words[i].word);
    }
    free(words);
    for(int j =0;j<crosswordHeight;j++){
        free(crosswords[j].line);
    }
    free(crosswords);
    free(line);
}

void fillInResult(LITERAL * result,int start, int end){
    for(int i = start; i < end; i++){
        result[i].checked = 1;
    }
}

int checkWord(LITERAL * literals,WORD * words,int i, int j,int sizeOfLine){
    return (literals[i].value == words[j].firstChar
            &&  words[j].size + i <= sizeOfLine
            && literals[i+words[j].size-1].value == words[j].lastChar) ? 1 : 0;
}

int checkWordBackWards(LITERAL * literals,WORD * words,int i, int j,int sizeOfLine) {
    return (literals[i].value == words[j].lastChar
            &&  words[j].size + i <= sizeOfLine
            && literals[i+words[j].size-1].value == words[j].firstChar) ? 1 : 0;
}

void createSubstring(int start, int end, LITERAL * source, char * result){
    int j = 0;
    for(int i = start; i<end;i++){
        result[j++] = source[i].value;
    }
    result[j] = '\0';
}

int compareStrings(char *str1,char *str2,int size){
    for(int i = 0;i<size; i++){
        if(str1[i] != str2[i])
            return 1;
    }
    return 0;
}

int compareStringsReverse(char *str1,char *str2,int size){
    for(int i = 0;i<size; i++){
        if(str1[i] != str2[size-i-1])
            return 1;
    }
    return 0;
}

void solveOneLine(LITERAL * literals, WORD *words,int sizeOfWords,int sizeOfLine,int crosswordsHeight){
    int criterium = crosswordsHeight != 0 ? crosswordsHeight : sizeOfLine;
    for(int i =0;i<criterium;i++){
        for(int j=0;j<sizeOfWords;j++){
            if(checkWord(literals,words,i,j,criterium) == 1){
                char substring[(i + words[j].size) - i +1];
                createSubstring(i,i + words[j].size,literals,substring);
                int res = compareStrings(words[j].word, substring, words[j].size);
                if(res  == 0){
                    //                    printf("found %s\n",words[j].word);
                    fillInResult(literals,i,i + words[j].size);
                    words[j].found = 1;
                }
            }
            if(checkWordBackWards(literals,words,i,j,criterium) == 1){
                char substring[(i + words[j].size) - i +1];
                createSubstring(i,i + words[j].size,literals,substring);
                int res = compareStringsReverse(words[j].word,substring,words[j].size);
                if(res == 0){
                    //                    printf("found %s\n",words[j].word);
                    fillInResult(literals,i,i + words[j].size);
                    words[j].found = 1;
                }
            }
        }
    }
}

void solveHorizontally(LINE * lines, WORD *words,int sizeOfWords,int lineHeight,int crosswordHeight){
    for(int j= 0;j<crosswordHeight;j++){
        LITERAL literals[lineHeight];
        for(int i= 0;i<lineHeight;i++){
            literals[i] = lines[j].line[i];
        }
        solveOneLine(literals,words,sizeOfWords,lineHeight,0);
        for(int i= 0;i<lineHeight;i++){
            if(literals[i].checked == 1){
                lines[j].line[i].checked = literals[i].checked;
            }
        }
    }
}

void changeAddress(LITERAL **source, LITERAL *data){
    *source = data;
}

void solveVertically(LINE * lines, WORD *words,int sizeOfWords,int lineHeight,int crosswordHeight){
    for(int x= 0;x<lineHeight;x++){
        LITERAL literals[crosswordHeight];
        for(int y = 0; y < crosswordHeight; y++)
        {
            literals[y] = lines[y].line[x];
        }
        solveOneLine(literals,words,sizeOfWords,lineHeight,crosswordHeight);
        for(int j = 0; j < crosswordHeight; j++)
        {
            if(literals[j].checked == 1 && lines[j].line[x].checked != 1) {
                lines[j].line[x].checked = literals[j].checked;
            }
        }

    }
}

void diagonalFromTop(LINE *lines, WORD *words, int sizeOfWords, int lineHeight, int crosswordHeight){
    int maxLength = lineHeight > crosswordHeight ? lineHeight : crosswordHeight;
    for (int k = 0; k <= 2 * (maxLength - 1); ++k) {
        LITERAL literals[crosswordHeight+1];
        int l = 0;
        for (int y = crosswordHeight - 1; y >= 0; --y) {
            int x = k - y;
            if (x >= 0 && y < lineHeight) {
                literals[l++] = lines[y].line[x];
            }
        }
        if(l > 0){
            literals[l].value = '\0';
            solveOneLine(literals,words,sizeOfWords,l,0);
            int m = 0;
            for (int y = crosswordHeight - 1; y >= 0; --y) {
                int x = k - y;
                if (x >= 0 && y < lineHeight) {
                    if(literals[m++].checked == 1){
                        lines[y].line[x].checked = literals[m-1].checked;
                    }
                }
            }
        }
    }
}

void diagonalFromBottom(LINE *lines, WORD *words, int sizeOfWords, int lineHeight, int crosswordHeight){
    int maxLength = lineHeight > crosswordHeight ? lineHeight : crosswordHeight;
    for (int k = 0; k <= (2 * (maxLength - 1)); k++) {
        LITERAL literals[crosswordHeight+1];
        int i = 0;
        for (int y = crosswordHeight - 1; y >= 0; y--) {
            int x = k - (crosswordHeight - y);
            if (x >= 0 && x < lineHeight) {
                literals[i++] = lines[y].line[x];
            }
        }
        if(i > 0){
            literals[i].value = '\0';
            solveOneLine(literals,words,sizeOfWords,i,0);
            int j = 0;
            for (int y = crosswordHeight - 1; y >= 0; y--) {
                int x = k - (crosswordHeight - y);
                if (x >= 0 && x < lineHeight) {
                    if (literals[j++].checked == 1){
                        lines[y].line[x].checked = literals[j-1].checked;
                    }
                }
            }
        }
    }
}

void enlargeArray(char ** arr, int * size){
    char * newArr = (char*) realloc(*arr,(*size) * 2 * sizeof(*newArr));
    if(newArr == NULL){
        printf("error, exiting!!");
        free(*arr);
        exit(5);
    } else {
        *size *= 2;
        *arr = newArr;
        newArr = NULL;
    }
}

void enlargeWords(WORD ** arr, int * size){
    WORD * newArr = (WORD*) realloc(*arr,(*size) * 2 * sizeof(*newArr));
    if(newArr == NULL){
        printf("error, exiting!!");
        free(*arr);
        exit(5);
    } else {
        *size = (*size) * 2;
        *arr = newArr;
        newArr = NULL;
    }
}

void enlargeCrossword(LINE ** arr, int * size){
    LINE * newArr = (LINE*) realloc(*arr,(*size) * 2 * sizeof(*newArr));
    if(newArr == NULL){
        printf("error, exiting!!");
        free(*arr);
        exit(5);
    } else {
        *size = (*size) * 2;
        *arr = newArr;
        newArr = NULL;
    }
}

int readFirstLine (int * lineSize, char ** line,int * sizeOfStack){
    int i = 0;
    while (1){
        int c = getchar();
        if(c == '\n'){
            break;
        }
        if(i == *sizeOfStack){
            enlargeArray(line,sizeOfStack);
        }
        (*line)[i++] = (char) c;
    }
    (*line)[i] = '\0';
    *lineSize = i;
    return i > 0 ? 0 : 1;
}

int readLine(int lineSize, LINE ** lines,int j,int * currentStackSize){
    int i = 0;
    char first = 0;
    (*lines)[j].line = (LITERAL *) malloc(lineSize * sizeof(*((*lines)[j].line)));
    while (1){
        int c = getchar();
        if(c == '\n' && !first){
            return 2;
        }
        if((c < 97 || c > 122)){
            if(c != 46){
                break;
            }
        }
        if(i == *currentStackSize){
            enlargeCrossword(lines, currentStackSize);
        }
        if(i > lineSize){
            break;
        }
        LITERAL tmp = {(char)c,0};
        (*lines)[j].line[i++] = tmp;
        first = 1;
    }
    (*lines)[j].size = i;
    if(i != lineSize){
        printf("Nespravny vstup.\n");
        exit(1);
    }
    return 0;
}

void readLines(int lineSize, LINE ** lines, int * crosswordHeight, int * currentStackSize){
    int i = 1;
    while(1) {
        if(readLine(lineSize,lines,i++,currentStackSize) != 0){
            break;
        }
        (*crosswordHeight)++;
    }
    *crosswordHeight = i - 1;
}

void fillFirstLine(int size, LINE *lines, char **characters){
    int i;
    lines[0].line = (LITERAL*) malloc(size * sizeof(*(lines[0].line)));
    for(i = 0;i < size;i++){
        LITERAL tmp = {(*characters)[i],0};
        lines[0].line[i] = tmp;
    }
    lines[0].size = i;
}

int readWord(int lineSize, WORD **words, int j, int * stackSize){
    int i = 0;
    int c;
    int isLast = 0;
    char * word = (char*) malloc(lineSize * sizeof(* word));
    while (1){
        c = getchar();
        if(c < 0){
            isLast = 1;
            break;
        }
        if((c < 97 || c > 122)){
            if(c != 46){
                break;
            }
        }
        if(*stackSize == i){
            enlargeWords(words,stackSize);
        }
        if(i > lineSize){
            free(word);
            return 1;
        }
        word[i++] = (char) c;
    }
    WORD tmp = {word,word[0],word[i-1],i,0};
    if (j >= *stackSize){
        enlargeWords(words,stackSize);
    }
    (*words)[j] = tmp;
    if(isLast){
        return 2;
    }
    if(i > lineSize) {
        printf("error, exiting!!");
        exit(3);
    }
    if(c == EOF){
        return 2;
    }
    return 0;
}

int readWords (WORD ** words, int size, int * wordsSize,int * stackSize){
    int i = 0;
    while (1){
        int res = readWord(size, words, i++, stackSize);
        if(res == 1){
            return 1;
        }else if(res == 2){
            *wordsSize = i-1;
            break;
        }
    }
    return 0;
}

void printResult(LINE * lines, int size, int height){
    printf("Vysledek:\n");
    int counter = 0;
    int printed = 0;
    for(int i = 0;i<height;i++)
        for(int j = 0;j<size;j++)
            if(lines[i].line[j].checked != 1 && lines[i].line[j].value != '.'){
                printed = 1;
                printf((++counter % 60) ? "%c" : "%c\n", lines[i].line[j].value);
            }
    if(printed){
        printf("\n");
    }
}

int foundAllWords(WORD * words,int sizeOfWords){
    int found = -1;
    for(int i=0;i < sizeOfWords;i++)
        if(words[i].found != 1){
            found = i;
        }
    return found;
}


int main() {
    printf("Zadejte osmismerku:\n");
    int lineWidth = 0;
    int wordsSize = 0;
    int foundIndex = -1;
    int crosswordHeight = 1;
    int sizeOfLine = 1;
    char * line =  (char*) malloc(sizeOfLine * sizeof(*line));
    int currentStackSizeWords = 1;
    //init words
    WORD * words = (WORD*) malloc(currentStackSizeWords * sizeof(*words));
    // read first line as array of chars
    if(readFirstLine(&lineWidth, &line, &sizeOfLine) == 1){
        printf("Nespravny vstup.\n");
        free(words);
        free(line);
        return 1;
    }
    // init crosswords array at size
    int currentStackSizeLines = 10000;
    LINE * crosswords = (LINE*) malloc(currentStackSizeLines * sizeof(*crosswords));
    // fill first line with array of chars
    fillFirstLine(lineWidth, crosswords, &line);
    // read from input rest and fill it into array
    readLines(lineWidth, &crosswords, &crosswordHeight,&currentStackSizeLines);
    // read all words we are looking for
    if (readWords(&words, lineWidth, &wordsSize, &currentStackSizeWords) == 1){
        printf("Nespravny vstup.\n");
        return 1;
    }

//    printf("read correctly %c\n", crosswords[0].line[0].value);//TODO - delete - debug purpose only

    solveHorizontally(crosswords, words, wordsSize, lineWidth, crosswordHeight);
    solveVertically(crosswords, words, wordsSize, lineWidth, crosswordHeight);
    diagonalFromBottom(crosswords, words, wordsSize, lineWidth, crosswordHeight);
    diagonalFromTop(crosswords, words, wordsSize, lineWidth, crosswordHeight);

    // check whether we found all words
    foundIndex = foundAllWords(words,wordsSize);
    if(foundIndex != -1){
        //        printf("index %d %d",foundIndex, wordsSize);//TODO - delete - debug purpose only
        // print which word we did not find
        printf("Slovo '%s' nenalezeno.\n", words[foundIndex].word);
        freeMemory(words,currentStackSizeWords,crosswords,currentStackSizeLines,line);
        return 1;
    }
    // print result
    printResult(crosswords, lineWidth, crosswordHeight);

    // empty memory
    freeMemory(words,currentStackSizeWords,crosswords,currentStackSizeLines,line);

    return 0;
}
